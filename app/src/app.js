

var app = angular.module('aknAtendimento', [
  'ngRoute', 'aknAtdController', 'aknMsgController', 'aknService',
  'aknAtdService', 'aknBltService', 'ngCpfCnpj','aknClockDirective',
  'ngMaterial', 'ngMessages', 'ngAnimate', 'ngSanitize'
]);

app.config(function($routeProvider, $locationProvider){

  $locationProvider.hashPrefix('');
  $routeProvider
  .when('/', {
    templateUrl: 'views/mensagem.html',
    controller: 'AknMsgController',
    controllerAs: 'ms'
  })
  .when('/atendimentos', {
    templateUrl : 'views/atendimentos.html',
    controller  : 'AknAtdController',
    controllerAs: 'at'
  })
  .otherwise({
    redirectTo : '/'
  });
});

// Pt_BR localization to calender.
app.config(function($mdDateLocaleProvider, $compileProvider){

  $compileProvider.preAssignBindingsEnabled(true);
  $mdDateLocaleProvider.months = [
    'Janeiro', 'Fevereiro', 'Março', 'Abril', 'Maio', 'Junho',
    'Julho', 'Agosto', 'Setembro', 'Outubro', 'Novembro', 'Dezembro'];
  $mdDateLocaleProvider.shortMonths = ['Jan', 'Fev', 'Mar', 'Abr', 'Mai', 'Jun', 'Jul', 'Ago', 'Set', 'Out', 'Nov', 'Dec'];
  $mdDateLocaleProvider.days = ['Domingo', 'Segunda', 'Terça', 'Quarta', 'Quinta', 'Sexta', 'Sábado'];
  $mdDateLocaleProvider.shortDays = ['Dom', 'Seg', 'Ter', 'Qua', 'Qui', 'Sex', 'Sab'];

  // Can change week display to start on Monday.
  $mdDateLocaleProvider.firstDayOfWeek = 1;

  $mdDateLocaleProvider.formatDate = function(date) {
    return moment(date).format('DD/MM/YYYY');
  };
});
