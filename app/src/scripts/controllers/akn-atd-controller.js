(function() {

    'use strict';

    angular.module('aknAtdController', []).controller('AknAtdController', Atendimento);

    function Atendimento($log, $scope, Atendimento) {
        var LOG = $log;
        LOG.info('Handler controller:', 'AknAtdController');
        $scope.titulo = 'Mensagens recebidas dos clientes';
        $scope.atendimentos = Atendimento.list();
        LOG.info('Handler atendimentos:', $scope.atendimentos);
    }

})();