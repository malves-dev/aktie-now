(function() {

    'use strict';

    angular.module('aknMsgController', []).controller('AknMsgController', Mensagem);

    function Mensagem($log, $scope, $mdDialog, Motivos, Atendimento, Boleto) {
        //var ms = this;
        var LOG = $log;
        LOG.info('Handler controller:', 'AknMsgController');
        $scope.titulo = 'Serviço de atendimento ao cliente';
        $scope.atendimentos = []; // Calls messages
        $scope.mensagem = {};
        $scope.boleto = {};

        // Date role
        $scope.dtVenc = new Date();

        $scope.minDate = new Date(
            $scope.dtVenc.getFullYear(),
            $scope.dtVenc.getMonth() - 0,
            $scope.dtVenc.getDate());

        $scope.maxDate = new Date(
            $scope.dtVenc.getFullYear(),
            $scope.dtVenc.getMonth(),
            $scope.dtVenc.getDate() + 15);

        LOG.info('Handler Date:', $scope.dtVenc);

        // CALL INTERNAL API MOTIVOS
        Motivos.list().then(function(data) {
            $scope.motivos = data;
        });

        $scope.cadastrarMotivo = cadastrarMotivo;

        function cadastrarMotivo(frm) {
            //LOG.info('Handler frm values:', frm);

            if (frm.$invalid) return;

            LOG.info('Handler $scope.mensagem:', $scope.mensagem);
            $scope.add($scope.mensagem); // add mesage

            $scope.boleto = Boleto.valor($scope.mensagem.dataVencimento); // get value

            LOG.info('Handler Boleto:', $scope.boleto);

            if ($scope.mensagem.mensagem !== undefined)
                $scope.showAlert();
            else
                $scope.showBoleto(JSON.stringify($scope.mensagem));

        }

        //
        $scope.add = function(obj) {
            Atendimento.add(obj);
        };

        //
        $scope.showAlert = function() {
            var message = 'Sua mensagem foi enviada, obrigado por utilizar nossos serviços.' +
                'Você receberá um retorno de nossos analistas através do email informado!';
            $mdDialog.show(
                $mdDialog.alert()
                //.parent(angular.element(document.querySelector('#popupContainer')))
                .clickOutsideToClose(true)
                .title('Envio de solicitação')
                .textContent(message)
                .ok('OK')
            );
        };

        //
        $scope.showBoleto = function(mensagem) {
            var message = 'Seu boleto foi gerado com os dados.';
            $mdDialog.show(
                $mdDialog.alert()
                //.parent(angular.element(document.querySelector('#popupContainer')))
                .clickOutsideToClose(true)
                .title('Boleto bancário')
                .textContent(mensagem)
                .ok('OK')
            ).then(function(answer) {

            }, function() {

            });
        };

    }
})();