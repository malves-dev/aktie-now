(function () {

	'use strict';

	angular.module('aknClockDirective', []).directive('clock', function($interval){
	  return{
		restrict: 'AE',
		link: function(scope, element, attrs){	 
			  var timer = $interval(function(){
				changeTime();
			  },1000);
		 
			  function changeTime(){
				 element.text((new Date()).toLocaleString());
			  }
		}
	  };
	});
	
})();