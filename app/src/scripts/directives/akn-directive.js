(function () {

	'use strict';

	angular.module('aknDirective', []).directive('aknDirective', AknDirective);

	function AknDirective($log) {
		return {
			templateUrl: 'views/atendimentos.html',
		};
	}

})();
