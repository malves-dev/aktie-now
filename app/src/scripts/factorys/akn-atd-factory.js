(function(){

'use strict';

angular.module('aknAtdService', []).factory("Atendimento", AknAtdFactory);

function AknAtdFactory($log){
    var atendimentos = [];

    var add = function(obj){
        atendimentos.push(obj);
    };

    var list = function(){
      return atendimentos;
    };

    return {
      add : add,
      list : list
    };
}

})();
