(function() {

    'use strict';

    angular.module('aknBltService', []).factory("Boleto", AknBoletoFactory);

    function AknBoletoFactory($log) {
        var LOG = $log;
        LOG.info('Handler boleto:', 'AknBoletoFactory');
        var valor = 1010.00; // Valor do boleto fixo

        var dataAtual = new Date();
        var dataMin = new Date(dataAtual.getFullYear(), dataAtual.getMonth(), dataAtual.getDate() + 3);
        var dataMax = new Date(dataAtual.getFullYear(), dataAtual.getMonth(), dataAtual.getDate() + 15);

        return {
            valor: function(data) {
                if (data === undefined) return valor;
                console.log('Data escolhida: ', data.getDate());

                if (data.getTime() > dataMin.getTime()) {
                    var dias = (dataMin.getDate() - data.getDate());
                    dias = dias < 0 ? dias * (-1) : dias;

                    console.log('Números de dias: ', dias);

                    for (var dia = 0; dia < dias; dia++) {
                        valor = valor + 2.5;
                        //console.log('Dia: ', dia, 'Valor: ', valor);
                    }
                    return valor;
                }
                return valor;
            }
        };
    }

})();