(function () {

	'use strict';

	angular.module('aknService', []).factory('Motivos', AknFactory);

	function AknFactory($log, $q, $http){
		var LOG = $log;
		LOG.info('Handler factory:', 'AknFactory');
		var urlApiMotivos = "http://localhost:3000/api/motivos.json";
		//var urlApiAtd = "https://myapi-e6f47.firebaseio.com/api/atd";//API remote test

		return {
			list: function() {
				var deffered  = $q.defer();
				$http({
					withCredentials: false,
					method : 'get', //
					url : urlApiMotivos,
					headers : {'Content-Type': 'text/json'}
				})
				.then(function success(response) {
					deffered.resolve(response.data);
				},
				function error (response){
					return error;
				});
				return deffered.promise;
			}
		};
	}
})();
