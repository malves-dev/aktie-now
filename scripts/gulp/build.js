'use strict';

var gulp = require('gulp');
var concat = require('gulp-concat');
var uglify = require('gulp-uglify');
var changed = require('gulp-changed');
var imagemin = require('gulp-imagemin');
var minifyCSS = require('gulp-minify-css');
var stripDebug = require('gulp-strip-debug');
var minifyHTML = require('gulp-minify-html');
var autoprefix = require('gulp-autoprefixer');

// Minify new images
gulp.task('imagemin', function() {
  var imgSrc = 'app/src/images/**/*',
  imgDst = 'build/src/images';
  gulp.src(imgSrc)
  .pipe(changed(imgDst))
  .pipe(imagemin())
  .pipe(gulp.dest(imgDst));
});

// Minify new or changed HTML pages
gulp.task('views', function() {
  var htmlSrc = 'app/src/views/**/*.html',
  htmlDst = 'build/src/views';
  gulp.src(htmlSrc)
  .pipe(changed(htmlDst))
  .pipe(minifyHTML())
  .pipe(gulp.dest(htmlDst));
});

// JS concat, strip debugging and minify
gulp.task('scripts', function() {
  gulp.src(['app/src/scripts/**/*.js'])
  .pipe(concat('script.js'))
  .pipe(stripDebug())
  .pipe(uglify())
  .pipe(gulp.dest('build/scripts/'));
});

// CSS concat, auto-prefix and minify
gulp.task('styles-concat-minify', function() {
  gulp.src(['app/src/styles/**/*.css'])
  .pipe(concat('styles.css'))
  .pipe(autoprefix('last 2 versions'))
  .pipe(minifyCSS())
  .pipe(gulp.dest('build/src/styles/'));
});
