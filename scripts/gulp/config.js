/**
 *  The main paths of your project handle these with care
 */
exports.paths = {
  app: 'app',
  dist: 'dist',
  tmp: '.tmp',
  e2e: 'e2e',
  js: 'js',
  css: 'css',
  html: 'html'
};