'use strict';

var gulp = require('gulp');
var jshint = require('gulp-jshint');
var path = require('path');

// JS hint task
gulp.task('jshint', function() {
  
console.log('>>>>>>>jshint>>>>>>');

gulp.src(['app/src/app.js','app/src/scripts/**/*.js'])
    .pipe(jshint())
    .pipe(jshint.reporter('default'));
});

// CSS hint task
gulp.task('csshint', function() {
gulp.src('app/src/styles/**/*.css')
    .pipe(jshint())
    .pipe(jshint.reporter('default'));
});

// HTML hint task
gulp.task('htmlhint', function() {
gulp.src('app/src/views/**/*.html')
    .pipe(jshint())
    .pipe(jshint.reporter('default'));
});
