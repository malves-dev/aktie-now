'use strict';

var path = require('path');
var gulp = require('gulp');
var inject = require('gulp-inject');
var wiredep = require('wiredep').stream;

//
gulp.task('inject', function () {

  console.log('>>>>>>>inject>>>>>>');

  var srcApp = gulp.src([
    path.join('app/src/app.js'),
    path.join('app/src/scripts/**/*.js'),
    path.join('app/src/styles/**/*.css')
  ], {read: false});

  gulp.src(path.join('app/src/index.html'))
  .pipe(inject(srcApp, {relative: true}))
  .pipe(wiredep({}))// Libs app
  .pipe(gulp.dest('app/src'));

});
