'use strict';

var path = require('path');
var gulp = require('gulp');
var browserSync = require('browser-sync');
var reload = browserSync.reload;
var jshint = require('gulp-jshint');

// Serve application
gulp.task('server', ['jshint', 'csshint'], function() {
  browserSync.init({
    server: {
      baseDir: 'app/src',
    },
  });
  //////////////////
});


gulp.task('reload', ['inject'], function() {
  reload;
});

// watch files for changes and reload
gulp.task('start', function() {
  browserSync({
    server: {
      baseDir: 'app/src'
    }
  });
  console.log('>>>>>Start-Application<<<<<<');
  gulp.watch(['watch'], {cwd: 'app/src'}, reload);
});
