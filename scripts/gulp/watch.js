'use strict';

var gulp = require('gulp');
var path = require('path');

// default gulp task
gulp.task('watch', function(done) {

	console.log('>>>>>>>watch>>>>>>');

	// watch for JS changes
	gulp.watch(['app/src/app.js','app/src/scripts/**/*.js'], function(event) {
		console.log('File ' + event.path + ' was ' + event.type + ', running tasks...');
	});

	// watch for CSS changes
	gulp.watch(['app/src/styles/**/*.css'], function(event) {
		console.log('File ' + event.path + ' was ' + event.type + ', running tasks...');
	});

	// watch for HTML changes
	gulp.watch(['app/src/views/**/*.html'], function(event) {
		console.log('File ' + event.path + ' was ' + event.type + ', running tasks...');
	});

});
