
describe('Motivos factory', function() {
	var motivos;

	var all = ["Elogio", "Sugestão", "Solicitação / Reclamação", "Re-emissão de boleto bancário"];

	// Before each test load our api.users module
	beforeEach(angular.mock.module('aknService'));

	// 
	beforeEach(inject(function(Motivos) {
	     motivos = Motivos.list();
		 console.log('AknFactory:', Motivos);
		 console.log('motivos:', motivos);
	}));

	// 
	it('should exist', function() {
	  expect(motivos).toBeDefined();
	});
  
});

describe('Realiza o boleto', function() {
	var boleto;
	
	var dataAtual = new Date();
	
	var valor = 1010.00; // primeiros 3 dias a partir da data de hoje

	var juros = 2.5; // valor a partir do 4° dia até o 15° dia
	
	var dataNext = new Date(
		dataAtual.getFullYear(),
		dataAtual.getMonth(),
		dataAtual.getDate() + 4
	);
	
	// service of boleto
	beforeEach(angular.mock.module('aknBltService'));
	
    // 
	beforeEach(inject(function(Boleto) {
         boleto = Boleto.valor(dataNext);
		 console.log('Valor do Boleto:', boleto);
	}));
	
	it('should have a controller', function() {
				
	});
	
	//
	// it('Calcula valores', function() {	
	  // expect(boleto).toBeDefined();
	// });
  
});

describe('Sorting the list of reasons on service module', function() {
	
  beforeEach(module('aknMsgController'));
  
  var $controller;

  // beforeEach(inject(function(_$controller_){
    // $controller = _$controller_;
  // }));
	
  it('sorts in descending order by default', function() {
    // your test assertion goes here
  });
});